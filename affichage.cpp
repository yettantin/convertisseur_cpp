#include "affichage.h"
#include "utils.h"
#include <iostream>
#include <string>

using namespace std;

void AfficherTitre() {
    cout << "------------------------------------" << endl;
	cout << "- CONVERTISSEUR CELCIUS FAHRENHEIT -" << endl;
	cout << "------------------------------------" << endl;
	}

void AuRevoir() {
    cout << "------------------------------------" << endl;
    cout << "-            A bientôt !           -" << endl;
    cout << "------------------------------------" << endl;
}

char choixTypeConversion() {

    char reponseTypeConversion('1');
    string reponse("reponse de l'utilisateur");

    cout << "Quelle conversion souhaitez vous faire?" << endl;
    cout << "1 - De celcius en fahrenheint" << endl;
    cout << "2 - De fahrenheint en celcius" << endl;

    do {
        // récupérer la réponse
        getline(cin, reponse);
        cin.clear();
        reponseTypeConversion = reponse.at(0);

        // s'assurer de la validité de la réponse
        if(reponseTypeConversion == '1' || reponseTypeConversion == '2') {
            return reponseTypeConversion;
        }
        else {
            cout << "Veuillez répondre 1 ou 2" << endl;
        }
    } while(reponseTypeConversion != '1' && reponseTypeConversion != '2');

    return reponseTypeConversion;
}

char choixRefaireConversion() {

    char faireConversion(' ');
    string reponse("reponse de l'utilisateur");

    cout << "Souhaitez vous faire une nouvelle convertion ? (O/N)" << endl;

    do {
        // récupérer la réponse
        cin.ignore();
        getline(cin, reponse);
        faireConversion = reponse.at(0);

            // s'assurer de la validité de la réponse
            if(faireConversion == 'O' || faireConversion == 'N') {
                return faireConversion;
            }
            else {
                cout << "Veuillez répondre O ou N" << endl;
            }
        } while(faireConversion != 'O' && faireConversion != 'N');

    return faireConversion;
}

double aConvertir() {

    bool valeurTeste;
    double valeurAConvertir;

    // Demander la valeur à convertir
    cout << "Entrer la température à convertir" << endl;

    // Recupère la valeur saisie par l'utilisateur
    cin >> valeurAConvertir;
    cin.clear();

    // Vérifie si la valeur est bien un double
    while(!valeurAConvertir) {

        cout << "Veuillez rentrer un nombre" << endl;

        cin.ignore();
        cin >> valeurAConvertir;
        cin.clear();
    }

    // Retourner la valeur à convertir
    return valeurAConvertir;
}

void AfficherResultatFar(double tempAConvertir,double tempConvertie) {
		cout << tempAConvertir << "°C équivaut à : " << tempConvertie << "°F." << endl;
	}
void AfficherResultatCel(double tempAConvertir,double tempConvertie) {
		cout << tempAConvertir << "°F équivaut à : " << tempConvertie << "°C." << endl;
	}
