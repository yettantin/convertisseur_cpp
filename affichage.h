#ifndef AFFICHAGE_H_INCLUDED
#define AFFICHAGE_H_INCLUDED

#include <string>

void AfficherTitre();
void AuRevoir();
char choixTypeConversion();
char choixRefaireConversion();
double aConvertir();
void AfficherResultatFar(double tempAConvertir,double tempConvertie);
void AfficherResultatCel(double tempAConvertir,double tempConvertie);

#endif // AFFICHAGE_H_INCLUDED
