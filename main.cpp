#include <iostream>
#include <string>
#include <math.h>
#include "affichage.h"
#include "calculateur.h"

using namespace std;

int main()
{
    // Déclarer les variables
    double tempAConvertir(0);
    char typeConversion(' ');
    char faireConversion(' ');

    AfficherTitre();

    do {
        // Choix du type de conversion
        typeConversion = choixTypeConversion();

        // Récupérer la valeur à convertir
        tempAConvertir = aConvertir();

        // Convertir la saisie
        if (typeConversion == '1') {
            double tempConvertieTraite = convertirCelEnFar(tempAConvertir);
            AfficherResultatFar(tempAConvertir, tempConvertieTraite);
        } else {
            double tempConvertieTraite = convertirFarEnCel(tempAConvertir);
            AfficherResultatCel(tempAConvertir, tempConvertieTraite);
        }

        // Demander de recommencer
		faireConversion = choixRefaireConversion();

    } while(faireConversion == 'O');

    AuRevoir();

    return 0;
}
