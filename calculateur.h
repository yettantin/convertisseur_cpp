#ifndef CALCULATEUR_H_INCLUDED
#define CALCULATEUR_H_INCLUDED

double convertirCelEnFar(double tempAConvertir);
double convertirFarEnCel(double tempAConvertir);

#endif // CALCULATEUR_H_INCLUDED
