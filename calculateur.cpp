#include <iostream>
#include <math.h>

double convertirCelEnFar(double tempAConvertir) {
    // Convertir des celcius en Fahrenheit
    double tempConvertie = (9.0/5)*tempAConvertir+32;

    // Arrondir le résultat à une décimale
    tempConvertie = (round(tempConvertie*10)*1.0)/10; // on multiplie par 1.0 pour typer en tant que double le numérateur

    return tempConvertie;
}

double convertirFarEnCel(double tempAConvertir) {
    // Convertir des fahrenheit en celcius
    double tempConvertie = ((tempAConvertir - 32) * 5) / 9;

    // Arrondir le résultat à une décimale
    tempConvertie = (round(tempConvertie*10)*1.0)/10;

    return tempConvertie;
}
