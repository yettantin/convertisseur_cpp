#include <iostream>
#include <string>
#include <limits>

using namespace std;


bool isNumeric( double & N ) {

    while ( ! ( std::cin >> N ))
    {
        if ( std::cin.eof() )
        {
            // ^D  (^Z sous windows); Fin du flux d'entree !
            return false;
        }
        else if ( std::cin.fail() )
        {

            std::cin.clear();
            std::cin.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
            return false;
        }

    }
    return true; // succès
}
